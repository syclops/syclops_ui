/**
 * @file
 * Hook Autocomplete functionality for this module.
 */
(function($) {
	// Extending Drupal.ACDB.prototype object for modifying search string.
	Drupal.ACDB.prototype.syclopsUISearch = function (searchString) {
		// Appending another field value on the search string.
		searchString = searchString+"/"+$("#edit-og-group-ref-und-0-default").val();
		return this.search(searchString);
	}
	// Populate popup.
	Drupal.jsAC.prototype.populatePopup = function () {
		// Remove popup.
  		if (this.popup) {
    		$(this.popup).remove();
  		}
		// Preparing object.
		this.selected = false;
		this.popup = document.createElement('div');
		this.popup.id = 'autocomplete';
		this.popup.owner = this;
  		$(this.popup).css({
			marginTop:this.input.offsetHeight +'px',
			width:(this.input.offsetWidth-4) +'px',
			display:'none'
		});
		$(this.input).before(this.popup);
		// Do search.
		this.db.owner = this;
		if (this.input.id=='edit-field-grant-ssh-config-und-0-nid') {
			this.db.syclopsUISearch(this.input.value);
		}else if(this.input.id=='edit-field-grant-user-und-0-uid') {
			this.db.syclopsUISearch(this.input.value);
		}else{			
			this.db.search(this.input.value);
		}
	}
	// Extending Drupal.behaviours object.
	Drupal.behaviors.rebindAutocomplete = function(context) {
    	// Unbind the behaviors to prevent multiple search handlers.
    	$("#edit-field-grant-ssh-config-und-0-nid").unbind('keydown').unbind('keyup').unbind('blur').removeClass('autocomplete-processed');
		$("#edit-field-grant-user-und-0-uid").unbind('keydown').unbind('keyup').unbind('blur').removeClass('autocomplete-processed');
    	// Rebind autocompletion with the new code.
    	Drupal.behaviors.autocomplete(context);
	}
})(jQuery);
