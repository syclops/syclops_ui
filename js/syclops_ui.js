/**
 * @file
 * Main javascript file for this module.
 */
(function($) {

	$.Syclops = {};
	$.Syclops.startElapedTimer = function() {				
				var minutes = 0;
        var hours = 0;
        var seconds = 0;

				$(".connection-status").html("Initializing Session....");
        if($("#webssh-terminal")[0]){
					$(".elapsed-timer-status").removeAttr("style");	
          setInterval(function(){
            seconds = seconds + 1;
            if( seconds == 59 ) {
              minutes = minutes + 1
              seconds = 0;
              if(minutes == 59) {
                hours = hours + 1;
                minutes = 0;
              }
            }
          var sec = ( seconds.toString().length == 1 ? "0"+seconds.toString() : seconds);
          var hh = (hours.toString().length == 1 ? "0"+hours.toString() : hours);
          var mm = (minutes.toString().length === 1 ? "0"+minutes.toString() : minutes );
          $('#elapsed-timer').text(hh+":"+mm+":"+sec);
          }, 1000);
       }
			setTimeout(function() {
          $(".connection-status").html("");
          $(".elapsed-timer-status").removeAttr("style");
					if($("#close-terminal-btn")[0]) {
						$(".connection-status").html("");
						$('#close-terminal-btn').removeAttr("style");
					}
				}, 1000);

	}

	// Extending Drupal.behaviours object.
	Drupal.behaviors.sessionCleanup = { 		
		attach:function(context,settings) {
			// Onclick event trigger for session cleanup button.
			if($('#session-cleanup')[0]) {
				$('#session-cleanup').on('click',function(e) {
					// Preventing default action of session cleanup button.
					e.preventDefault();
					// Hiding session cleanup button.
					$(this).hide('2000');
					// Setting variables.				
					sessionCleanupInfo = $('#session-cleanup-info');
					// Setting message.
					sessionCleanupInfo.text('Session cleanup is in progress. Please wait...');
					// Setting variables.
					sessionCleanupURL = $(this).attr('href');
					// Triggering ajax request to cleanup the session on server end.
					$.ajax({
						// Request method.
						type:'GET',
						// Http url to be requested.
						url:sessionCleanupURL,
						// Return data type.
						dataType:'json',
						// Callback to be triggered on successful request.
						success:function(data) {
							// Session info element emptied.
							sessionCleanupInfo.empty();												
							switch(data.status) {						
							case 'success':
								// Setting message.
                  		sessionCleanupInfo.text('Session cleanup done. Reconnecting...');
								// Reloading the page on successful session cleanup.
								setTimeout(function() {
									location.reload();
								},1000);
							break;						
							case 'failure':
								// Setting message.
                        sessionCleanupInfo.text('Session cleanup failed. Please try again by reloading the page.'); 									
							break;
						}
					},
					// Callback to be triggered on error request.
					error:function(data) {
						// Session info element emptied.
						sessionCleanupInfo.empty();
						// Setting message.				
						sessionCleanupInfo.text('An unexpected error occured. Please try again by reloading the page.');					
					}				
				});
			});			
		}
	}
	};
   // Extending Drupal.behaviours object.
   Drupal.behaviors.sshConfigPopulate = {
      attach:function(context,settings) {
			// Connection Name field object.
			var connectionName = $('#edit-title');
			// Machine User field object.
			var machineUser = $('#edit-field-ssh-config-machine-user-und-0-value');
			// Machine Name field object.
			var machineName = $('#edit-field-ssh-config-machine-und-0-value');

                        var sessionLimit = $('#edit-field-syclops-config-max-c-session-und-0-value');

			// Form object.
			var form = connectionName.parents('form');
			// Nullifying placeholder for Connection Name field object.
			connectionName.attr('placeholder','');
			// OnBlur event for Connection Name field object.
			connectionName.on('blur',function(event) {
				if(connectionName.val()=='') {
					connectionName.attr('placeholder',machineUser.val()+'@'+machineName.val());
				}				
			});
			// OnInput event for Machine User field object.
			machineUser.on('input',function(event) {
				connectionName.attr('placeholder',machineUser.val()+'@'+machineName.val());
			});
			//  OnInput event for Machine Name field object.
			machineName.on('input',function(event) {
				connectionName.attr('placeholder',machineUser.val()+'@'+machineName.val());
			});

			// OnSubmit event for Form object.
			form.submit(function() {
				if(connectionName.val()=='') {
					connectionName.val(connectionName.attr('placeholder'));
				}			
			});
		}
	};


 Drupal.behaviors.closeActiveTerminal = {
      attach:function(context,settings) {
   		 if($(".close-terminal", context)[0]) {
					$(".close-terminal", context).on("click",function(e) {
						if($("#webssh-terminal", context)[0]){
							document.getElementById("webssh-terminal").contentWindow.GateOne.Syclops.forceClose();
						}
					  
					});
				}
  	}
	};

})(jQuery);
