<header id="navbar" role="banner" class="navbar navbar-fixed-top navbar-inverse">
	<div class="navbar-inner">
		<div class="container-fluid">
      	<!-- .btn-navbar is used as the toggle for collapsed navbar content -->
      	<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        		<span class="icon-bar"></span>
        		<span class="icon-bar"></span>
        		<span class="icon-bar"></span>
      	</a>
      	<?php if (!empty($logo)): ?>
            <a class="logo pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
               <img src="<?php print $logo; ?>" style="width:45px; height:auto;" alt="<?php print t('Home'); ?>"></img>
            </a>
			<?php endif; ?>
      	<?php if (!empty($site_name)): ?>
            <span class="pull-left">
               <?php
                  // Extracting og_group_ref value.
                  $og = field_get_items('node',$node,'og_group_ref');
                  $og = reset($og);
                  // Loading node entity.
                  $og = node_load($og['target_id']);
               ?>
               <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" class="brand"><?php print $_SERVER['HTTP_HOST']; ?></a>
               <a href="<?php print url('node/'.$og->nid,array('absolute'=>TRUE)); ?>" class="brand"><i class="icon-angle-right"></i>&nbsp;&nbsp;<?php print $og->title; ?></a>
               <?php if (!empty($title)): ?>
                  <a href="<?php print url('node/'.$node->nid,array('absolute'=>TRUE)); ?>" class="brand"><i class="icon-angle-right"></i>&nbsp;&nbsp;<?php print $title; ?></a>
               <?php endif; ?>
            </span>
      	<?php endif; ?>
			<?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
        		<div class="nav-collapse collapse">
          		<nav role="navigation">
            		<?php if (!empty($primary_nav)): ?>
              			<?php print render($primary_nav); ?>
            		<?php endif; ?>
            		<?php if (!empty($secondary_nav)): ?>
              			<?php print render($secondary_nav); ?>
            		<?php endif; ?>
            		<?php if (!empty($page['navigation'])): ?>
              			<?php print render($page['navigation']); ?>
            		<?php endif; ?>
                  <?php
                     if(user_is_logged_in()) {
                        global $user;
                        print '<ul class="nav pull-right">';
                        print '<li><a><i class="icon-signin"></i>&nbsp;Logged in: '.$user->name.'</a></li>';
                        if(in_array('site admin',$user->roles)||($user->uid==1)) {
                           print '<li><a href="admin"><i class="icon-cog"></i>&nbsp;Administer</a></li>';
                        }                    
                        print '</ul>';
                     }
                  ?>
          		</nav>
				</div>
      	<?php endif; ?>
    	</div>
	</div>
</header>
<div class="container-fluid" id="main-container">
 	<div class="row-fluid">
		<?php if (!empty($page['sidebar_first'])): ?>
      	<aside class="span2" role="complementary" id="sidebar-first">
        		<?php print render($page['sidebar_first']); ?>
      	</aside>  <!-- /#sidebar-first -->
    	<?php endif; ?>  
    	<section class="<?php print _syclops_content_span($columns); ?>" id="main-content">  
      	<?php print render($title_prefix); ?>
         <?php if (!empty($title)): ?>
           	<!--<div class="page-header row-fluid">
               <div class="span6" style="padding:13px 0px;"><?php print $title; ?></div>
               <div class="span6">
                  <?php
                     global $base_url;
                     $path = drupal_get_path('theme','syclops');
                     print '<a href="http://syclops.org" target="_blank" class="pull-right"><img src="/'.$path.'/logo.png" style="width:70px;height:50px;"></img></a>';
                  ?> 
               </div>
            </div>-->
         <?php endif; ?>
      	<?php print render($title_suffix); ?>
         <?php if (!empty($page['highlighted'])): ?>
            <div class="highlighted row-fluid"><?php print render($page['highlighted']); ?></div>
         <?php endif; ?>
      	<?php print $messages; ?>
      	<?php if (!empty($page['help'])): ?>
        		<div class="well"><?php print render($page['help']); ?></div>
      	<?php endif; ?>
			<?php
				$flag = flag_get_flag('parent_archive');
				if(!$flag->is_flagged($node->nid)) {
            	module_load_include('inc','syclops_ui','includes/syclops_ui.layout');					
            	print render(syclops_ui_layout('session'));
					print render($page['content']);
				} else {
               $message = block_load('block',11);
               print drupal_render(_block_get_renderable_array(_block_render_blocks(array($message))));				
				}
			?>
		</section>
    	<?php if (!empty($page['sidebar_second'])): ?>
      	<aside class="span3" role="complementary" id="sidebar-second">
        		<?php print render($page['sidebar_second']); ?>
      	</aside>  <!-- /#sidebar-second -->
    	<?php endif; ?>
  	</div>
</div>
<footer class="footer container">
	<?php print render($page['footer']); ?>
</footer>
