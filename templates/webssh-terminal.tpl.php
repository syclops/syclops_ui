<?php
/**
 * @file
 * Rendered on 'WebSSH Terminal' Menu Page Callback.
 */
?>
<article class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
	<div class="row">
		<div class="col-md-12">
        <?php
				global $base_url;
				// Checking if active session exists for current user.
				if($webssh_session) {
					$grant_status = 'error';					
				}
				// Checking grant status.
				switch ($grant_status) {
					case 'activated':						
						print '<center>';
						print '<iframe id="webssh-terminal" src='.$base_url.'/webssh/'.$grant_nid.'/connect width=100% frameborder=0 allowTransparency=true></iframe>';
						print '</center>';
					break;
					case 'disabled':
						print '<center><h3>Grant is currently disabled</h3></center>';
					break;
					case 'archived':
						print '<center><h3>Grant was archived</h3></center>';
					break;
					case 'error':
						print '<center><h3>An active session exists. Please cleanup the session</a> to launch a new terminal.</h3></center>';
						print '<center><a href="'.$base_url.'/session/'.$session_id.'/cleanup" id="session-cleanup" class="btn btn-sm btn-danger">Cleanup Session</a></center>';
						print '<center><span id="session-cleanup-info"></span></center>';
					break;
				}
         ?>
		</div>
	</div>
	<nav class="navbar navbar-default navbar-fixed-bottom">
		<div class="col-md-12">
   		<?php
      		// Constructing table headers.
      		$header = array(
         		array('data'=>t('Project')),
         		array('data'=>t('Connection')),         		
         		array('data'=>t('User')),
						array('data'=>t('Status')),
			array('data'=>t('Action'))
      		);
      		// Constructing table rows.
      		$rows[] = array(
         		array('data'=>t('<strong>Project:</strong> '.$project_title)),
         		array('data'=>t('<strong>Connection:</strong> '.$ssh_config_title)),
						array('data'=>t('<span class="connection-status">Click Terminal Icon to start session</span>')),
						array('data'=>t('<span class="elapsed-timer-status" style="display:none;"><strong>Elapsed Time : </strong><span id="elapsed-timer">00.00.00</span><span>')),
						array('data'=>t('<span id="close-terminal-btn" style="display:none;" class="btn btn-sm btn-default btn-danger close-terminal">Close</span> ')), //$user_name
      		);
      		// Printing table.
      		print theme('table',array('header'=>NULL,'rows'=>$rows,'attributes'=>array()));
   		?>
   	</div>
	</nav> 
</article>
