<?php
/**
 * @file
 * Getting started related functions.
 */
function syclops_ui_getting_started() {
	global $base_url;
	$url = $base_url.'/admin/getting-started';
	$content ="<h1>Welcome to Syclops</h1>";
 	$content .="<p>This quick guide will take you through the immediate first steps needed to configure your Syclops installation. You can always access this page by visiting <a href=".$url.">".$url."</a></p>";
	$content .="Follow these steps to get started adding users, and creating your first project in Syclops.";
	$content .="<h3>1. Create Users</h3>";
	$conteent.="<p>Syclops is a closed system to be only used by authorized persons. Since the number of personnel involved in Systems Administration in any organization is limited, Syclops enforces a strict security policy requireing users to be manually added by an site administrators. This ensures only relevant persons have access to the system.</p>";
	$content .=" <p>Adding users is easy:</p>";
	$content .="<ul style='list-style-type: none;'><li>1. Go to the <strong>People</strong> administration page by clicking <a href=".$base_url."/admin/people>here</a></li>";
	$content .="<li>2. Click on the <strong>+ Add User</strong> link at the top to add a new user.</li>";
	$content .="<li>3. Fill all fields marked as mandatory. You can optionally have a welcome email sent to the user to manually activate their account. Otherwise, the account is activated on save.</li>";
	$content .="<li>4. If you wish to give this user the right to create new projects, select the <strong>project creator</strong> role under Roles. If you wish to create a new site administrator, grant the <strong>site admin</strong> role.</li>";
  $content .="<li><p><strong>Warning</strong>. Do not grant site admin rights to users unnecessarily.</p></li>";
	$content.="<li><p>That's it. The user account is live, and can be added to a project. You can add all your users at the same time, or configure them later on by clicking the <strong>Administer</strong> icon at the top navigation bar, and then going to the <strong>People</strong> administration section.</p></li></ul>";
	$content .="<h3>2. Set SMTP settings</h3>";
	$content .="Syclops sends certain email notifications to users, such account creation, password reset notifications, etc. In order for this to happen, the SMTP mail settings need to be configured to allow for outbound mail to sent.</p>";
	$content .="- Configure the SMTP settings by going to this <a href=".$base_url."/admin/config/system/smtp>link</a>.";
	$content .="<p>That's it. Your Syclops installation is ready to be used.</p><hr>";

	$content .="<h3>Creating projects, connections, etc., and regular use of Syclops</h3>";
	$content .="
	<p>Syclops is built around the concept of project workspaces. Each workspace is an isolated container which restricts members of that project to access only the connections defined in the project. This isolation allows for multiple projects to be centrally managed simultaneously while ensuring access restrictions.
	</p>
	<p>By design, Syclops <strong>site-admins</strong> accounts can not be used for daily use. You need to login as a <strong>project-creator</strong> to create projects, and start managing Syclops. <strong>project creator</strong> accounts are meant for regular use and are mapped to individual users. <strong>site-admin</strong> accounts are only to be used for accessing the backend system for troubleshooting and updating configuration.
</p>";
	$content .="<strong>To create a project, and start using Syclops</strong>
	<p>
		- Logout as admin and login with a <strong>project-creator</strong> enabled account.<br>
		- Head over to the Dashboard and click on the <strong>Create Project</strong> link<br>
		- After entering a project name, you are taken to the project page to start adding users, connections and grants to projects, and start securely working with your SSH connections.<br>
	</p>
	<p><strong>Note:</strong> For more information on how to administer a project, and for more details on managing users, we have a more indepth getting started guide published online.
	</p>";
	$content .="<a href='http://bit.ly/syclops_gsguide'>Click here to view and download the guide</a>";
	return $content;
}


