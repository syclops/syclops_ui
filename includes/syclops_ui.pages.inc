<?php
/**
 * @file
 * Menu related functions.
 */
/**
 * Menu Page Callback for 'WebSSH Terminal'.
 */
function syclops_ui_webssh($grant) {
	if(syclops_ui_allow_connect($grant->nid))
	 {
		 $response = array(
         'error_title' => 'User Error',
         'error_code' => '1001',
         'error_details' => 'User has active session',
       );
     //drupal_goto('webssh/error', array('query' => $response));
	 }

   // Extracting field_grant_user value.
   $grant_user = field_get_items('node',$grant,'field_grant_user');
   $grant_user = reset($grant_user);
   // Extracting field_grant_status value.
   $grant_status = field_get_items('node',$grant,'field_grant_status');
   $grant_status = reset($grant_status);
	// Load user entity.
	$user = user_load($grant_user['uid']);
   // Extracting og_group_ref value.
   $og = field_get_items('node',$grant,'og_group_ref');
   $og = reset($og);
	// Load node entity.
	$og = node_load($og['target_id']);
	// Extracting field_grant_ssh_config value.
   $ssh_config = field_get_items('node',$grant,'field_grant_ssh_config');
   $ssh_config = reset($ssh_config);
	// Load node entity.
	$ssh_config = node_load($ssh_config['nid']);
   // Params to be passed to template.
   $params = array();
   $params['user_id'] = $user->uid;
   $params['user_name'] = $user->name;
   $params['project_nid'] = $og->nid;
   $params['project_title'] = $og->title;
   $params['ssh_config_nid'] = $ssh_config->nid;
   $params['ssh_config_title'] = $ssh_config->title;
   $params['grant_nid'] = $grant->nid;
   $params['grant_title'] = $grant->title;
   $params['grant_status'] = $grant_status['value'];
	$params['webssh_session'] = false;
   
   // Selecting existing sessions for the user.
   $webssh_session = db_select('syclops_webssh_sessions','n')
      ->fields('n')
      ->condition('user_id',$user->uid,'=')
      ->execute()
      ->fetchAssoc();

   //$max_session = $ssh_config->field_ssh_config_machine['und'][0]['value'];
   //$grant_connection = node_load($webssh_session['grant_id']);
   //$cid = $grant_connection->field_grant_ssh_config['und'][0]['value'];
   //if($ssh_config->nid != $cid) {
   // if($max_session == 0) {
   //    $params['webssh_session'] = true;
   //    $params['session_id'] = $webssh_session['session_id'];
   //}

	// Checking existing webssh session for current user.
   if($webssh_session!=NULL) {
      if($webssh_session['grant_id'] == $grant->nid) {
         $params['webssh_session'] = true;
         $params['session_id'] = $webssh_session['session_id'];
      }
   }

   // Get module path.
   $module_path = drupal_get_path('module','syclops_ui');
   // Adding javascript.
	drupal_add_js($module_path.'/js/syclops_ui.js');
	// Returning custom theme hook.
   return theme('webssh_terminal',$params);
}
/**
 * Menu Page Callback for 'Close Session'.
 */
function syclops_ui_session_close_form($form,$form_state) {
   $form = array();
   // Session log.
   $form['session_log'] = array(
      '#type' => 'textarea',
      '#title' => t('Session Log'),
      '#description' => t('Enter the Session Log.'),
      '#default_value' => NULL,     
      '#required' => TRUE
   );
	// Submit button.
   $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
      '#submit' => array('syclops_ui_session_close_submit')
   );
	return $form;
}
/**
 * Submit validation for 'Close Session' form.
 */
function syclops_ui_session_close_submit($form,&$form_state) {	
	try {
		// Loading node entity.
		$session = node_load(arg(1));
		// Preparing object for edit.
		node_object_prepare($session);
		// Setting field values.
		$session->field_session_log[$session->language][0]['value'] = $form_state['values']['session_log'];
		$session->field_session_status[$session->language][0]['value'] = 'closed';
		// Saving node.
		node_save($session);
      // Extracting field_session_grant value.
      $grant = field_get_items('node',$session,'field_session_grant');
      $grant = reset($grant);
      // Load node entity.
      $grant = node_load($grant['nid']);
      // Extracting og_group_ref value.
      $og = field_get_items('node',$grant,'og_group_ref');
      $og = reset($og);
      // Loading node entity.
      $og = node_load($og['target_id']);
      // Extracting field_grant_ssh_config value.
      $ssh_config = field_get_items('node',$grant,'field_grant_ssh_config');
      $ssh_config = reset($ssh_config);
      // Load node entity.
      $ssh_config = node_load($ssh_config['nid']);
      // Extracting field_grant_user value.
      $user = field_get_items('node',$grant,'field_grant_user');
      $user = reset($user);
      // Load user entity.
      $user = user_load($user['uid']);
      // Loading .inc files.
      module_load_include('inc','syclops_log','includes/syclops_log.api');
      // Array of objects.
      $objects = array();
      // Array of values.
      $params = array();
      // Building data.
      $params['uid'] = $user->uid;
      $objects['field_msg_project_ref'] = $og;
      $objects['field_msg_ssh_config_ref'] = $ssh_config;
		$objects['field_msg_grant_ref'] = $grant;
		$objects['field_msg_session_ref'] = $session;
      // Logging message.
      syclops_log_message('msg_close_session',$params,$objects);
		// Removing session from db.
		db_delete('syclops_webssh_sessions')
			->condition('session_id',$session->nid)
			->execute();
		// Success message.
		drupal_set_message(t('Session has been closed successfully.'),'success');
		// Redirecting to project dashboard page. 
		$form_state['redirect'] = url('node/'.$session->nid,array('absolute'=>TRUE));
	} catch(Exception $error) {
		// Error message.
		drupal_set_message(t('Could not close session.'),'error');		
	}
}
/**
 * Menu Page Callback for 'Cleanup Session'.
 */
function syclops_ui_session_cleanup($session) {
	global $user;
	$data = array();
   try {
		// Preparing object for edit.
      node_object_prepare($session);
      // Setting field values.		
      $session->field_session_log[$session->language][0]['value'] = 'The session had been closed automatically';
      $session->field_session_status[$session->language][0]['value'] = 'closed';	
			//force close template 
			$fields = array();
      $fields['id'] = $session->nid;
      $fields['upn'] = $user->name;
      $fields['session_id'] =  $session->field_session_id[$session->language][0]['value'];
   
      // Saving node.
      node_save($session);
      // Extracting field_session_grant value.
      $grant = field_get_items('node',$session,'field_session_grant');
      $grant = reset($grant);
      // Load node entity.
      $grant = node_load($grant['nid']);
			$fields['grant_id'] = $grant->nid;

      // Extracting og_group_ref value.
      $og = field_get_items('node',$grant,'og_group_ref');
      $og = reset($og);
      // Loading node entity.
      $og = node_load($og['target_id']);
      // Extracting field_grant_ssh_config value.
      $ssh_config = field_get_items('node',$grant,'field_grant_ssh_config');
      $ssh_config = reset($ssh_config);
      // Load node entity.
      $ssh_config = node_load($ssh_config['nid']);
      // Extracting field_grant_user value.
      $user = field_get_items('node',$grant,'field_grant_user');
      $user = reset($user);
      // Load user entity.
      $user = user_load($user['uid']);
      // Loading .inc files.
      module_load_include('inc','syclops_log','includes/syclops_log.api');
      // Array of objects.
      $objects = array();
      // Array of values.
      $params = array();
      // Building data.
      $params['uid'] = $user->uid;
      $objects['field_msg_project_ref'] = $og;
      $objects['field_msg_ssh_config_ref'] = $ssh_config;
      $objects['field_msg_grant_ref'] = $grant;
      $objects['field_msg_session_ref'] = $session;

			//create force close session playback templates.
      _syclops_ui_create_force_close_session_playback($fields);

      // Logging message.
      syclops_log_message('msg_cleanup_session',$params,$objects);
      // Removing session from db.
      db_delete('syclops_webssh_sessions')
         ->condition('session_id',$session->nid)
         ->execute();    
		// Setting status.
		$data['status'] = 'success';
   } catch(Exception $error) {
      // Setting status.
		$data['status'] = 'failure';
	} 
   // Sending JSON data.
   drupal_json_output($data);	
}
/**
 * Menu Page Callback for fore close the session.
 */
function syclops_ui_session_force_close($session) {
	global $user;
  $data = array();
   try {
                // Preparing object for edit.
      node_object_prepare($session);
      // Setting field values.          
      $session->field_session_log[$session->language][0]['value'] = 'The session had been closed automatically';
      $session->field_session_status[$session->language][0]['value'] = 'closed';

			//force close field data
			$fields = array();
			$fields['id'] = $session->nid;
			$fields['upn'] = $user->name;
			$fields['session_id'] =  $session->field_session_id[$session->language][0]['value'];
      // Saving node.
      node_save($session);
      // Extracting field_session_grant value.
      $grant = field_get_items('node',$session,'field_session_grant');
      $grant = reset($grant);
      // Load node entity.
      $grant = node_load($grant['nid']);
			$fields['grant_id'] = $grant->nid;
      // Extracting og_group_ref value.
      $og = field_get_items('node',$grant,'og_group_ref');
      $og = reset($og);
      // Loading node entity.
      $og = node_load($og['target_id']);
      // Extracting field_grant_ssh_config value.
      $ssh_config = field_get_items('node',$grant,'field_grant_ssh_config');
      $ssh_config = reset($ssh_config);
      // Load node entity.
      $ssh_config = node_load($ssh_config['nid']);
      // Extracting field_grant_user value.
      $user = field_get_items('node',$grant,'field_grant_user');
      $user = reset($user);
      // Load user entity.
      $user = user_load($user['uid']);
      // Loading .inc files.
      module_load_include('inc','syclops_log','includes/syclops_log.api');
      // Array of objects.
      $objects = array();
      // Array of values.
      $params = array();
      // Building data.
      $params['uid'] = $user->uid;
      $objects['field_msg_project_ref'] = $og;
      $objects['field_msg_ssh_config_ref'] = $ssh_config;
      $objects['field_msg_grant_ref'] = $grant;
      $objects['field_msg_session_ref'] = $session;

			//create force close session playback templates.
			_syclops_ui_create_force_close_session_playback($fields);
 
      // Logging message.
      syclops_log_message('msg_cleanup_session',$params,$objects);
      // Removing session from db.
      db_delete('syclops_webssh_sessions')
         ->condition('session_id',$session->nid)
         ->execute();
      // Setting status.
			drupal_goto('dashboard');
						
   } catch(Exception $error) {
      // Setting status.
      $data['status'] = 'failure';
   }
}

/**
 * Menu Page Callback for 'Download Session'.
 */
function syclops_ui_session_download($session) {
	global $base_url;
	// Extracting field_session_id value.
	$session_id = field_get_items('node',$session,'field_session_id');
	$session_id = reset($session_id);
	// Filename of the playback.
	$pattern = '/https?:\/\//i';
  $replace = '';
  $file_name = preg_replace($pattern, $replace, $base_url);
  $file_name .= '_'.$session->nid.'.html';
	$file_path .= 'public://playbacks/'.$file_name;
	$uri = "";

  //Checks file name is new or old
  if(file_exists($file_path)) {
		$uri = $file_path;
	} else {
	  $file_name = "";
		$file_name = $session_id['value'].'.html';
		// Playback file storage path.
		$directory = 'public://playbacks';
		// Constructing drupal uri.
		$uri = $directory.'/'.$file_name;
	}

	// Checking for file exists.
	if(file_exists($uri)) {
		// Setting http headers.
		$headers = array(
			'Content-Type'=>'text/html',
			'Content-Disposition'=>'attachment;filename='.$file_name.';',
			'Content-Length'=>filesize(drupal_realpath($uri)),
			'Cache-Control'=>'private',
			'Expires'=>0,
			'Pragma'=>'public'
		);
		// Transfering file.
		file_transfer($uri,$headers);
	}else{
		// Error message.
		drupal_set_message(t('Playback not available.'),'error');
		// Redirecting to current path.
		drupal_goto('dashboard');		
	}
}

/**
 * Menu Page Callback for 'Activate WebSSH'.
 */
function syclops_ui_webssh_activate($grant) {
	// Changing grant status.
	$grant->field_grant_status[LANGUAGE_NONE][0]['value'] = 'activated';
	// Saving node.
	node_save($grant);
	// Setting message.
	drupal_set_message(t('Grant has been activated successfully.'));
	// Extracting og_group_ref value.	
   $og = field_get_items('node',$grant,'og_group_ref');
   $og = reset($og);
	// Redirecting to current path.
	drupal_goto(url('node/'.$og['target_id'],array('absolute'=>TRUE)));	
}
/**
 * Menu Page Callback for 'Deactivate WebSSH'.
 */
function syclops_ui_webssh_deactivate($grant) {
	// Changing grant status.
   $grant->field_grant_status[LANGUAGE_NONE][0]['value'] = 'disabled';
	// Saving node.
   node_save($grant);
	// Setting message.
   drupal_set_message(t('Grant has been deactivated successfully.'));
   // Extracting og_group_ref value.   
   $og = field_get_items('node',$grant,'og_group_ref');
   $og = reset($og);
   // Redirecting to current path.
   drupal_goto(url('node/'.$og['target_id'],array('absolute'=>TRUE)));
}

/**
 * Menu Page callback function for session error.
 */
function syclops_ui_session_error($response = NULL) {
 return theme('session_error_page',
   array(
         'error_type' => ($_GET['error_type']) ? $_GET['error_type'] : NULL,
         'error_title' => ($_GET['error_title']) ? $_GET['error_title'] : NULL,
         'error_code' => ($_GET['error_code']) ? $_GET['error_code'] : NULL,
         'error_message' => ($_GET['error_message']) ? $_GET['error_message'] : NULL,
         'error_details' => ($_GET['error_details']) ? $_GET['error_details'] : NULL
         )
   );
}


/**
 * Helper function to triggers force close callback.
 */
function _syclops_ui_create_force_close_session_playback($fields = array()) {
	global $base_url;
	$webssh_host = variable_get('webssh_host');
	$webssh_port = variable_get('webssh_port');
	//Gateone base url
	$url = "https://".$webssh_host.":".$webssh_port;
	$end_point = $url."/syclops/forceclose";
	//Gatone access credential
	$fields['api_key'] = variable_get('webssh_api_key');
	$fields['timestamp'] = time().'000';
  $fields['signature_method'] = 'HMAC-SHA1';
  $fields['api_version'] = '1.0';
	// Generating signature.
	$fields['signature'] =
			hash_hmac('sha1',
				$fields['api_key'].$fields['upn'].$fields['timestamp'],
				variable_get('webssh_secret_key')
	);

	//storage path
	$pattern = '/https?:\/\//i';
  $replace = '';
  $file_name = preg_replace($pattern, $replace, $base_url);
  $file_name .= '_'.$fields['id'].'.html';

	//curl init
	$ch = curl_init();
	$fields['storage_path'] = drupal_realpath('public://playbacks').'/'.$file_name;
  $fields['notify_url'] = $base_url.'/webssh/'.$fields['grant_id'].'/session/notify';
	$postvars = '';
	//creating post variable
	foreach($fields as $key=>$value) {
    $postvars .= $key .'='.$value.'&';
	}
	//Removing whitespace
	rtrim($postvars, '&');
	curl_setopt($ch, CURLOPT_URL, $end_point);
	curl_setopt($ch,CURLOPT_POST,count($fields));
	curl_setopt($ch,CURLOPT_POSTFIELDS,$postvars);
	//Adding this option to avoid ssl error
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	//Makes curl request
	$data = curl_exec($ch);
	$err = curl_error($ch);
	if($err) {
		 watchdog('action', 'Force close template creation callback has been faild');
	}
	//close curl request
	curl_close($ch);
}

/**
 * Helper function uses to block account action.
 */
function syclops_ui_block_user_action($uid) {
 global $user;
 if($uid) {
   $account = user_load($uid);
   if($account->status == 1) {
      $account = user_save($account, array('status' => 0));
      watchdog('action', 'Blocked user %name.', array('%name' => $account->name));
      drupal_goto('user');
   } else {
      $account = user_save($account, array('status' => 1));
      watchdog('action', 'Activated user %name.', array('%name' => $account->name));
      drupal_goto('user');
   }
 }
}
