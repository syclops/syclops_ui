<?php
/**
 * @file
 * References related functions.
 */
/**
 * Menu Page Callback for 'Syclops UI References'.
 */
function syclops_ui_reference_autocomplete($entity_type,$bundle,$field_name,$string='',$param='') {
	// Creating map.
	$map = array(
		'field_grant_ssh_config' => array('view'=>'syclops_grant','display'=>'references_3','entity_type'=>'node'),
		'field_grant_user' => array('view'=>'syclops_user','display'=>'references_1','entity_type'=>'user')
	);
	// Getting field information.
	$field = field_info_field($field_name);
	// Field instance data.
	$instance = field_info_instance($entity_type,$field_name,$bundle);
	// Validating $param.
	preg_match_all("/\(([A-Za-z0-9 ]+?)\)/",$param,$output);	
	$param = end(end($output));
	// Adding field settings.
	$field['settings']['view']['view_name'] = $map[$field_name]['view'];
	$field['settings']['view']['display_name'] = $map[$field_name]['display'];
	$field['settings']['view']['args'][] = $param;
	// Autocomplete options.
	$options = array(
		'string' => $string,
		'match' => $instance['widget']['settings']['autocomplete_match'],	
		'ids' => array(),
		'limit' => 0
	);
	// Getting the references.
	$references = call_user_func_array($map[$field_name]['entity_type'].'_reference_potential_references',array($field,$options));
	// Building suggestions for autocomplete popup.
	$matches = array();
	// Setting tag on reference field.
	$tag = NULL;
	switch ($map[$field_name]['entity_type']) {
		case 'node':
			$tag = 'nid';
		break;
		case 'user':
			$tag = 'uid';
		break;
	}
	foreach($references as $id=>$row) {
		$suggestion = preg_replace('/<a href="([^<]*)">([^<]*)<\/a>/','$2',$row['rendered']);
		// Add a class wrapper for a few required CSS overrides.
		$matches[$row['title']." [$tag:$id]"] = '<div class="reference-autocomplete">'.$suggestion.'</div>';
	}
	// Rendering JSON output.
	drupal_json_output($matches);	
}
