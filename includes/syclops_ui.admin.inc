<?php
/**
 * @file
 * Admin related functionalities.
 */
/**
 * Menu Page Callback for 'WebSSH Settings'.
 */
function syclops_ui_webssh_settings_form($form,$form_state) {
	$form = array();
	// WebSSH API key.
	$form['webssh_api_key'] = array(
		'#type' => 'textfield',
		'#title' => t('API Key'),
      '#description' => t('Enter the WebSSH API Key.'),
      '#default_value' => variable_get('webssh_api_key',''),
		'#element_validate' => array('syclops_ui_webssh_validate_api_key'),
		'#maxlength' => 60,		
      '#size' => 60,
      '#required' => TRUE
   );
	// WebSSH Secret key.	
	$form['webssh_secret_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Secret Key'),
      '#description' => t('Enter the WebSSH Secret Key.'),
      '#default_value' => variable_get('webssh_secret_key',''),
		'#element_validate' => array('syclops_ui_webssh_validate_secret_key'),
		'#maxlength' => 60,
      '#size' => 60,
      '#required' => TRUE		
	);
	// WebSSH Hostname or IP.
   $form['webssh_host'] = array(
      '#type' => 'textfield',
      '#title' => t('Host'),
      '#description' => t('Enter the WebSSH Hostname or IP.'),
      '#default_value' => variable_get('webssh_host','localhost'),
      '#element_validate' => array('syclops_ui_webssh_validate_host'),
      '#maxlength' => 60,
      '#size' => 60,
      '#required' => TRUE
   );
	// WebSSH Port.
	$form['webssh_port'] = array(
		'#type' => 'textfield',
		'#title' => t('Port'),
		'#description' => t('Enter the WebSSH Port.'),
      '#default_value' => variable_get('webssh_port',7443),
		'#element_validate' => array('syclops_ui_webssh_validate_port'),
		'#maxlength' => 60,
      '#size' => 60,
      '#required' => TRUE
	);
	return system_settings_form($form);		
}
/**
 * Validates WebSSH API Key field.
 */
function syclops_ui_webssh_validate_api_key($element,&$form_state) {
	// Getting API key value.
   $value = $element['#value'];
	// Checking API key pattern.
   if(!preg_match("/^[a-zA-Z0-9]*$/",$value)) {
      form_error($element, t('This API Key is invalid.'));
   }
}
/**
 * Validates WebSSH Secret Key field.
 */
function syclops_ui_webssh_validate_secret_key($element,&$form_state) {
	// Getting Secret key value.
   $value = $element['#value'];
	// Checking Secret key pattern.
   if(!preg_match("/^[a-zA-Z0-9]*$/",$value)) {
      form_error($element, t('This Secret key is invalid.'));
   }
}
/**
 * Validates WebSSH Host field.
 */
function syclops_ui_webssh_validate_host($element,&$form_state) {
	// Getting Hostname or IP value.
   $value = $element['#value'];
	// IP pattern.
   $ip_regex = "/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/";
	// Hostname pattern.
   $hostname_regex = "/^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/";
	// Checking Hostname pattern.
   if(!preg_match($hostname_regex,$value)) {
      $hostname_error = TRUE;
   }
	// Checking IP pattern.
   if(!preg_match($ip_regex,$value)) {
      $ip_error = TRUE;
   }
	// Checking for errors.
   if(isset($hostname_error) && isset($ip_error)) {
      form_error($element,t('This Hostname or IP is invalid.'));
   }
}
/**
 * Validates WebSSH Port field.
 */
function syclops_ui_webssh_validate_port($element,&$form_state) {
	// Getting Port value.
   $value = $element['#value'];
	// Setting Port range.
   $port_range = range(1,61000);
	// Checking for range and numeric values.
   if(!is_numeric($value) || !in_array($value,$port_range)) {
      form_error($element,t('This Port is invalid.'));
   }
}
