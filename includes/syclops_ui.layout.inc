<?php
/**
 * @file
 * Menu related functions.
 */
/**
 * Layout processor.
 */
function syclops_ui_layout($mode='dashboard') {
   // Checking if anonymous user.
   if(user_is_anonymous()) {
		// Getting theme path.
		$path = drupal_get_path('theme','syclops');
   	// Setting page title.
   	drupal_set_title('Sign In',CHECK_PLAIN);		
   	// User login block.
   	$login = array(
      	'element' => array(
         	'#tag' => 'div',
         	'#attributes' => array(
            	'class' => array('col-md-6', 'jumbotron')
        		),
				'#value' => drupal_render(drupal_get_form('user_login'))
      	)
   	);			
   	// Printing some simple text values.
   	$getting_started = array(
      	'element' => array(
         	'#tag' => 'div',
         	'#attributes' => array(
            	'class' => 'col-md-6'
         	),
         	'#value' => '<a href="http://syclops.org" target="_blank"><center><img src="/'.$path.'/logo.png"></img></center></a>'
      	)
   	);
   	// Building output.
   	$output = array(
      	'element' => array(
         	'#children' => theme_html_tag($login).theme_html_tag($getting_started),
         	'#attributes' => array('class'=>'row')
      	)
   	);
   	// Returning themed output.
   	return theme_container($output);	
	}
	global $user;
	switch ($mode) {
		case 'dashboard':
			// Setting page title.
			drupal_set_title('Dashboard',CHECK_PLAIN);
			// Getting layout template.
			$layout = file_get_contents(drupal_get_path('module','syclops_ui').'/layouts/'.$mode.'.json');			
		break;
		case 'project':			
   		// Getting og roles.
   		$og_roles = og_get_user_roles('node',arg(1),$user->uid,FALSE);
   		// Getting membership.
   		$og_member = og_is_member('node',arg(1),'user',$user);
			// Checking for administrator role.
   		if(in_array('project admin',$og_roles)) {				
         	// Getting layout template.
         	$layout = file_get_contents(drupal_get_path('module','syclops_ui').'/layouts/admin-'.$mode.'.json');								
   		} elseif($og_member) {
	         // Getting layout template.
				$layout = file_get_contents(drupal_get_path('module','syclops_ui').'/layouts/'.$mode.'.json');
			} else {
				// Using drupal's access denied.
				drupal_access_denied();
				return;
			}												
		break;
		case 'ssh-config':
		case 'session':
			$node = node_load(arg(1));
   		// Extracting og_group_ref value.
   		$og = field_get_items('node',$node,'og_group_ref');
   		$og = reset($og);
         // Getting og roles.
         $og_roles = og_get_user_roles('node',$og['target_id'],$user->uid,FALSE);
         // Getting membership.
         $og_member = og_is_member('node',$og['target_id'],'user',$user);
         // Checking for administrator role.
         if(in_array('project admin',$og_roles)||$og_member) {
         	// Getting layout template.
         	$layout = file_get_contents(drupal_get_path('module','syclops_ui').'/layouts/'.$mode.'.json');
			} else {
				// Using drupal's access denied.
				drupal_access_denied();
				return;
			}
		break;
	}
	// Layout template to array.
	$layout = json_decode($layout,true);
	// Constructing layout.	
	$output = syclops_ui_layout_construct($layout);
	return $output;
}
/**
 * Layout construct.
 */
function syclops_ui_layout_construct($layout) {
	$output = NULL;
	foreach($layout as $row) {
		$content = NULL;
		foreach($row as $block) {
			// Generating block.
			$content .= syclops_ui_block_construct($block);
		}
		// Creating row variables.		
		$variables = array(
			'element' => array(
				'#children' => $content,
				'#attributes' => array('class'=>'row')
			)
		);
		// Building container.			
		$output .= theme_container($variables);	
		unset($content);	
	}
	return $output;	
}
/**
 * Block construct.
 */
function syclops_ui_block_construct($block) {	
	// Building block title.
	$title = array(
      'element' => array(
         '#tag' => 'h3',
         '#attributes' => array(
            'class' => array('block-title'),
         ),
         '#value' => $block['title']
      )
   );
	// Building block content.
	$content = array(
      'element' => array(
         '#tag' => 'div',
         '#attributes' => array(
				'class' => array('block'),
         ),
			'#value' => theme_html_tag($title).views_embed_view($block['view_name'],$block['display_id'])
			#'#value' => views_embed_view($block['view_name'],$block['display_id'])
      )		
	);	
	// Setting up float value.
	switch ($block['float']) {
		case 'right':
   		$wrapper = array(
      		'element' => array(
         		'#tag' => 'div',
         		'#attributes' => array(
            		'class' => $block['width']
         		),
         		'#value' => theme_html_tag($content)
      		)
   		);			
		break;
		case 'left':
         $wrapper = array(
            'element' => array(
               '#tag' => 'div',
               '#attributes' => array(
                  'class' => $block['width']
               ),
               '#value' => theme_html_tag($content)
            )
         );
		break;
	}
	// Returning entire block.
	return theme_html_tag($wrapper);	
}
